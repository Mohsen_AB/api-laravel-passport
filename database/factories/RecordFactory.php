<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class RecordFactory extends Factory
{
    /** @return array */
    public function definition(): array
    {
        return [
            // 'device_id' => '1',
            'blood_pressure' => $this->faker->numberBetween(40, 150),
            'oxygen_level' => $this->faker->numberBetween(80, 100),
            'sleep_duration' => now()->timestamp,
        ];
    }
}
