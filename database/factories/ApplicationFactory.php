<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory */
class ApplicationFactory extends Factory
{
    /** @return array */
    public function definition(): array
    {
        return [
            // 'user_id' => '1',
            'name' => $this->faker->name,
        ];
    }
}
