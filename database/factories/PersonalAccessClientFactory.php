<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PersonalAccessClientFactory extends Factory
{
    public function definition()
    {
        return [
            'client_id' => 1,
        ];
    }
}
