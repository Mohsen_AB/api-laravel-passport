<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory */
class DeviceFactory extends Factory
{
    /** @return array */
    public function definition(): array
    {
        return [
            // 'application_id' => '1',
            'name' => $this->faker->name
        ];
    }
}
