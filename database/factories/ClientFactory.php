<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ClientFactory extends Factory
{
    /** @return array */
    public function definition(): array
    {
        return [
            // 'user_id' => '1',
            'name' => $this->faker->company(),
            'secret' => Str::random(40),
            'redirect' => $this->faker->url(),
            'personal_access_client' => true,
            'password_client' => true,
            'revoked' => false,
        ];
    }
}
