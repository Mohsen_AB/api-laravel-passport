<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->foreignId('device_id')->constrained()->onDelete('cascade');
            $table->integer('blood_pressure');
            $table->integer('oxygen_level');
            $table->integer('sleep_duration');
            $table->timestamps();
        });
    }

    /**
     * @param Blueprint $table
     *
     * @return void
     */
    public function down(Blueprint $table)
    {
        $table->dropForeign(['device_id']);
        Schema::dropIfExists('records');
    }
};
