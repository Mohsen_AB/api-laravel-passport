<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\Device;
use App\Models\PersonalAccessClient;
use App\Models\Record;
use App\Models\User;
use Illuminate\Database\Seeder;
use Laravel\Passport\Client;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::factory()->create()
            ->each(function ($user) {
                $client = Client::factory()->create([
                    'user_id' => $user,
                    'redirect' => 'http://localhost/callback',
                    'provider' => 'users',
                    'personal_access_client' => true,
                    'password_client' => true
                ]);

                $personalAccessClient = PersonalAccessClient::factory()->create(['client_id' => $client->id]);
                $user->createToken('AuthToken')->accessToken;
                $application = Application::factory()->create(['user_id' => $user]);
                $device = Device::factory()->create(['application_id' => $application]);
                $records = Record::factory(rand(1, 20))->create(['device_id' => $device]);
            });
    }
}
