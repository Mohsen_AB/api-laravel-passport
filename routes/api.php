<?php

use App\Http\Controllers\Api\Application\DeleteController as ApplicationDeleteController;
use App\Http\Controllers\Api\Application\StoreController as ApplicationStoreController;
use App\Http\Controllers\Api\Application\UpdateController as ApplicationUpdateController;
use App\Http\Controllers\Api\Application\ShowController as ApplicationShowController;
use App\Http\Controllers\Api\Device\StoreController as DeviceStoreController;
use App\Http\Controllers\Api\Device\UpdateController as DeviceUpdateController;
use App\Http\Controllers\Api\Device\ShowController as DeviceShowController;
use App\Http\Controllers\Api\Device\DeleteController as DeviceDeleteController;
use App\Http\Controllers\Api\User\IndexController as UserIndexController;
use App\Http\Controllers\Api\Record\IndexController as RecordIndexController;
use App\Http\Controllers\Api\Record\ShowController as RecordShowController;
use App\Http\Controllers\Api\Record\DeleteController as RecordDeleteController;
use App\Http\Controllers\Api\Record\StoreController as RecordStoreController;
use App\Http\Controllers\Api\Record\UpdateController as RecordUpdateController;
use App\Http\Controllers\Api\Login\StoreController as LoginStoreController;
use App\Http\Controllers\Api\Logout\StoreController as LogoutStoreController;
use App\Http\Controllers\Api\Registration\StoreController as RegistrationStoreController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->prefix('v1')->group(function () {

    Route::get('/user', [UserIndexController::class, 'index']);

    Route::get('/application/{application}', [ApplicationShowController::class, 'show']);
    Route::post('/application', [ApplicationStoreController::class, 'store']);
    Route::put('/application/{application}', [ApplicationUpdateController::class, 'update']);
    Route::delete('/application/{application}', [ApplicationDeleteController::class, 'destroy']);

    Route::get('/device/{device}', [DeviceShowController::class, 'show']);
    Route::post('/device', [DeviceStoreController::class, 'store']);
    Route::put('/device/{device}', [DeviceUpdateController::class, 'update']);
    Route::delete('/device/{device}', [DeviceDeleteController::class, 'destroy']);

    Route::get('/record', [RecordIndexController::class, 'index']);
    Route::get('/record/{record}', [RecordShowController::class, 'show']);
    Route::post('/record', [RecordStoreController::class, 'store']);
    Route::put('/record/{record}', [RecordUpdateController::class, 'update']);
    Route::delete('/record/{record}', [RecordDeleteController::class, 'destroy']);

    Route::post('/logout', [LogoutStoreController::class, 'store']);

});

Route::post('/login', [LoginStoreController::class, 'store']);
Route::post('/register', [RegistrationStoreController::class, 'store']);