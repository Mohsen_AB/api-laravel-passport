<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceResource extends JsonResource
{
    /**
     * @param $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => (string)$this->id,
            'type' => 'Device',
            'attributes' => [
                'application_id' => (string)$this->application_id,
                'name' => $this->name,
                'created_at' => $this->created_at->diffForHumans(),
            ]
        ];
    }
}
