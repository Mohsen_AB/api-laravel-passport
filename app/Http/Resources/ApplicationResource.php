<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationResource extends JsonResource
{
    /**
     * @param $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => (string)$this->id,
            'type' => 'Application',
            'attributes' => [
                'user_id' => (string)$this->user_id,
                'name' => $this->name,
                'created_at' => $this->created_at->diffForHumans(),
            ]
        ];
    }
}