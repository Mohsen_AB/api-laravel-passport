<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecordeResource extends JsonResource
{
    /**
     * @param $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => (string)$this->id,
            'type' => 'Record',
            'attributes' => [
                'device_id' => (string)$this->device_id,
                'blood_pressure' => $this->blood_pressure,
                'oxygen_level' => $this->oxygen_level,
                'sleep_duration' => $this->sleep_duration,
                'created_at' => $this->created_at->diffForHumans(),
            ]
        ];
    }
}
