<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecordRequest extends FormRequest
{
    /** @return bool */
    public function authorize(): bool
    {
        return true;
    }

    /** @return array */
    public function rules(): array
    {
        return [
            'blood_pressure' => ['required', 'min:1', 'max:300', 'integer'],
            'oxygen_level' => ['required', 'min:1', 'max:300', 'integer'],
            'sleep_duration' => ['required', 'min:1', 'max:300', 'integer'],
        ];
    }
}