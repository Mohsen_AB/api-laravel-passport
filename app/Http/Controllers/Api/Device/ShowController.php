<?php

namespace App\Http\Controllers\Api\Device;

use App\Http\Controllers\Controller;
use App\Http\Resources\DeviceResource;
use App\Models\Device;

class ShowController extends Controller
{
    /**
     * @param Device $device
     *
     * @return DeviceResource
     */
    public function show(Device $device): DeviceResource
    {
        return new DeviceResource($device);
    }
}
