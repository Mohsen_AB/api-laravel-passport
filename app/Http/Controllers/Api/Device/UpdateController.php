<?php

namespace App\Http\Controllers\Api\Device;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDeviceRequest;
use App\Http\Resources\DeviceResource;
use App\Models\Device;

class UpdateController extends Controller
{
    /**
     * @param StoreDeviceRequest $request
     * @param Device $device
     *
     * @return DeviceResource|void
     */
    public function update(StoreDeviceRequest $request, Device $device)
    {
        $validated = $request->validated();

        if ($device->exists) {

            $device->update($validated);
            return new DeviceResource($device);
        }
    }
}
