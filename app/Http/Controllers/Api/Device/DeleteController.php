<?php

namespace App\Http\Controllers\Api\Device;

use App\Http\Controllers\Controller;
use App\Models\Device;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class DeleteController extends Controller
{
    /**
     * @param Device $device
     *
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(Device $device): \Illuminate\Http\Response|ResponseFactory
    {
        if ($device->exists) {

            $device->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        }

        abort(Response::HTTP_NOT_FOUND);
    }
}
