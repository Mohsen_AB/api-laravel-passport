<?php

namespace App\Http\Controllers\Api\Device;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDeviceRequest;
use App\Http\Resources\DeviceResource;
use App\Models\Device;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends Controller
{
    /**
     * @param StoreDeviceRequest $request
     *
     * @return DeviceResource
     */
    public function store(StoreDeviceRequest $request): DeviceResource
    {
        $validated = $request->validated();
        $applicationId = auth()->user()->application->id ?: abort(Response::HTTP_INTERNAL_SERVER_ERROR);
        $device = Device::create(array_merge(['application_id' => $applicationId], $validated));

        return new DeviceResource($device);
    }
}
