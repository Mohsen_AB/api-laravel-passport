<?php

namespace App\Http\Controllers\Api\Login;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLoginRequest;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function store(StoreLoginRequest $request)
    {
        $validated = $request->validated();

        if (!Auth::attempt($validated)) {
            return response(['message' => 'Invalid login credentials.']);
        }

        return auth()->user()->createToken('Auth Token')->accessToken;
    }
}
