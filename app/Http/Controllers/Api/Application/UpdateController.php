<?php

namespace App\Http\Controllers\Api\Application;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreApplicationRequest;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;

class UpdateController extends Controller
{
    public function update(StoreApplicationRequest $request, Application $application)
    {
        $validated = $request->validated();

        if ($application->exists) {
            $application->update($validated);

            return new ApplicationResource($application);
        }
    }
}
