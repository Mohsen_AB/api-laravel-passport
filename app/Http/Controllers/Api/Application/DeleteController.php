<?php

namespace App\Http\Controllers\Api\Application;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class DeleteController extends Controller
{
    /**
     * @param Application $application
     *
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(Application $application): \Illuminate\Http\Response|ResponseFactory
    {
        if ($application->exists) {

            $application->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        }

        abort(Response::HTTP_NOT_FOUND);
    }
}
