<?php

namespace App\Http\Controllers\Api\Application;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreApplicationRequest;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;

class StoreController extends Controller
{
    /**
     * @param StoreApplicationRequest $request
     *
     * @return ApplicationResource
     */
    public function store(StoreApplicationRequest $request): ApplicationResource
    {
        $validated = $request->validated();
        $application = Application::create(array_merge($validated, ['user_id' => auth()->user()->id]));

        return new ApplicationResource($application);
    }
}
