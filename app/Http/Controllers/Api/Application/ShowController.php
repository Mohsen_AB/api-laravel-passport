<?php

namespace App\Http\Controllers\Api\Application;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApplicationResource;
use App\Models\Application;

class ShowController extends Controller
{
    /**
     * @param Application $application
     *
     * @return ApplicationResource
     */
    public function show(Application $application): ApplicationResource
    {
        return new ApplicationResource($application);
    }
}
