<?php

namespace App\Http\Controllers\Api\Logout;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /** @param Request $request */
    public function store(Request $request)
    {
        $request->user()->tokens()->delete();
    }
}
