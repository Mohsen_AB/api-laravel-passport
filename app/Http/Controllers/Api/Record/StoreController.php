<?php

namespace App\Http\Controllers\Api\Record;


use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRecordRequest;
use App\Http\Resources\RecordeResource;
use App\Models\Record;

class StoreController extends Controller
{
    /**
     * @param StoreRecordRequest $request
     *
     * @return RecordeResource
     */
    public function store(StoreRecordRequest $request): RecordeResource
    {
        $validated = $request->validated();
        $application = Record::create(array_merge($validated, ['device_id' => auth()->user()->application->device->id]));

        return new RecordeResource($application);
    }
}
