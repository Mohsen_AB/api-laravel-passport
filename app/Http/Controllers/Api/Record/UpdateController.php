<?php

namespace App\Http\Controllers\Api\Record;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRecordRequest;
use App\Http\Resources\RecordeResource;
use App\Models\Record;

class UpdateController extends Controller
{
    /**
     * @param StoreRecordRequest $request
     * @param Record $record
     *
     * @return RecordeResource
     */
    public function update(StoreRecordRequest $request, Record $record)
    {
        $validated = $request->validated();

        if ($record->exists) {
            $record->update($validated);

            return new RecordeResource($record);
        }
    }
}
