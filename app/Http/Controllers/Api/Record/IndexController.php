<?php

namespace App\Http\Controllers\Api\Record;

use App\Http\Controllers\Controller;
use App\Http\Resources\RecordResourceCollection;
use App\Models\Record;

class IndexController extends Controller
{
    /** @return RecordResourceCollection */
    public function index(): RecordResourceCollection
    {
        return new RecordResourceCollection(Record::all());
    }
}
