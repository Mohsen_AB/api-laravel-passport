<?php

namespace App\Http\Controllers\Api\Record;

use App\Http\Controllers\Controller;
use App\Http\Resources\RecordeResource;
use App\Models\Record;

class ShowController extends Controller
{
    /**
     * @param Record $record
     *
     * @return RecordeResource
     */
    public function show(Record $record): RecordeResource
    {
        return new RecordeResource($record);
    }
}
