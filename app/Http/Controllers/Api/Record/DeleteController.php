<?php

namespace App\Http\Controllers\Api\Record;

use App\Http\Controllers\Controller;
use App\Models\Record;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

class DeleteController extends Controller
{
    /**
     * @param Record $record
     *
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(Record $record): \Illuminate\Http\Response|ResponseFactory
    {
        if ($record->exists) {

            $record->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        }

        abort(Response::HTTP_NOT_FOUND);
    }
}
