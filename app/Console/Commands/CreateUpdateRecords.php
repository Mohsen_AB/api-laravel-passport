<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateUpdateRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:records';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'creating and updating sensor records';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Command::SUCCESS;
    }
}
