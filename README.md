## About this project

This application runs on Laravel Sail

- Make sure you have installed docker on your system (https://laravel.com/docs/9.x#getting-started-on-windows).
- When the application is cloned on your system. First you should run this code. You can read more here [sail](https://laravel.com/docs/9.x/sail#installing-composer-dependencies-for-existing-projects)
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```
- Make ```.env.example``` into ```.env```
- Make database connections
- Run ```sail artisan key key:generate```
- Run ```npm install``` and ```npm run dev```
- Run ```composer install```
- Run ```sail artisan migrate```
- Run ```sail artisan db:seed```
- Run ```sail artisan schedule:work```
- Run ```sail artisan passport:install```


## Model Relationships
- A user has one application 
- An application has one device
- A device has many records


## Schedule Command
- ```create:records``` command runs every minute to re 